#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run by root."
fi

# Set hostname
read -p "Enter a hostname for this server:" -n 1
echo
echo $REPLY > /etc/hostname
hostname -F /etc/hostname
sed -i "2i$REPLY" /etc/hosts
  
# Set timezone
dpkg-reconfigure tzdata

# Update packages
apt-get update
apt-get -y upgrade

# Install packages
apt-get -y install nginx php7.0-fpm php7.0-curl php7.0-gd php7.0-mcrypt mariadb-server mariadb-client git
mysql_secure_installation

# Configure Nginx
cp nginx/nginx.conf /etc/nginx/nginx.conf

# Install phpMyAdmin
read -p "Install phpMyAdmin? (y/n):" -n 1
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  apt-get -y install phpmyadmin
  cp nginx/pma /etc/nginx/sites-available/pma
  ln -s /etc/nginx/sites-available/pma /etc/nginx/sites-enabled/pma
fi

# Install Postfix
read -p "Will this server send mail? (y/n):" -n 1
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  apt-get -y install postfix
fi

# Create user
read -p "Enter a username:" user
adduser $user
passwd $user

# Create Virtual Host
read -p "Create a virtual host? (y/n):" -n 1
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  read -p "Site URL:" server_name
  echo
  default_dir="/home/$user/public"
  read -p "Site root directory [$default_dir]: "
  vhost_root=${REPLY:-$default_dir}
  echo
  mkdir -p $vhost_root
  vhost_file=/etc/nginx/sites-available/$server_name
  cp nginx/vhost vhost_file
  ln -s vhost_file /etc/nginx/sites-enabled/$server_name
  sed -i 's/SERVER_NAME/$server_name/g' $vhost_file
  sed -i 's/HOST_ROOT/$vhost_root/g' $vhost_file
fi

# Install Composer
read -p "Install Composer? (y/n):" -n 1
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  php composer-setup.php --install-dir=/usr/local/bin --filename=composer
  rm composer-setup.php
fi

# Install Drush
read -p "Install Drush? (y/n):" -n 1
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  php -r "readfile('https://s3.amazonaws.com/files.drush.org/drush.phar');" > drush
  chmod +x drush
  mv drush /usr/local/bin
fi
